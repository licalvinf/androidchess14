package com.example.androidchess14;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class WatchActivity extends AppCompatActivity {
    private List<RecordedGame> gameList = MainActivity.getGamesList();
    private ListView gamesView;
    private static List<String> moveList;
    Button dateButton;
    Button titleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch);
        dateButton= findViewById(R.id.date_button);
        titleButton=findViewById(R.id.title_button);
        gamesView = findViewById(R.id.games_view);
        if(gamesView==null){
            //Log.d("Debug", "gameView Null");
        }
        if(gameList==null){
            //Log.d("Debug", "gameList Null");
        }
        else{
            //Log.d("Debug", "gameList not Null");
        }
        ArrayAdapter viewAdapter = new ArrayAdapter<>(this, R.layout.recordedgame, gameList);

        gamesView.setAdapter(viewAdapter);
        gamesView.setClickable(true);
        gamesView.setOnItemClickListener((parent, view, position, id) -> {
            RecordedGame clickedGame = (RecordedGame) parent.getItemAtPosition(position);
            setMoveList(clickedGame.moveList);
            Intent i = new Intent(getApplicationContext(), ReplayActivity.class);
            startActivity(i);
        });
        dateButton.setOnClickListener(v -> {
            //Log.d("First Game", gameList.get(0).toString());
            Collections.sort(gameList,new DateComparator());
            viewAdapter.notifyDataSetChanged();
        });
        titleButton.setOnClickListener(v -> {
            Collections.sort(gameList,new TitleComparator());
            viewAdapter.notifyDataSetChanged();
        });


    }

    public static void setMoveList(List<String> moveList) {
        WatchActivity.moveList = moveList;
    }
    public static List<String> getMoveList(){
        return moveList;
    }
    public class TitleComparator implements Comparator<RecordedGame>{
        @Override
        public int compare(RecordedGame o1, RecordedGame o2) {
            return o1.title.compareToIgnoreCase(o2.title);
        }
    }
    public class DateComparator implements Comparator<RecordedGame>{
        @Override
        public int compare(RecordedGame o1, RecordedGame o2) {
            return -1*(o1.date.compareTo(o2.date));
        }
    }
}