package com.example.androidchess14;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.FragmentManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;


public class PlayActivity extends AppCompatActivity{
    protected static HashMap<String, ImageView> uiMap;
    ImageView testImg;
    Button start_game, draw, resign, ai_button, undo, quit, yes_save, no_save, save_game_btn;
    RadioGroup promotionChoices;
    protected static String left_command;
    protected static String right_command;
    protected Board b1;
    protected static ArrayList<String> recorded_moves;
    protected Boolean can_undo;
    protected Board lastSavedBoard;
    protected Board tempBoard;
    protected static char promoteChoice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        left_command = null;
        right_command = null;
        b1 = null;
        start_game = findViewById(R.id.startgame_button);
        draw = findViewById(R.id.draw);
        resign = findViewById(R.id.resign);
        ai_button = findViewById(R.id.ai);
        undo = findViewById(R.id.undo);
        quit = findViewById(R.id.quit_game);
        yes_save = findViewById(R.id.yes_save);
        no_save = findViewById(R.id.no_save);
        save_game_btn = findViewById(R.id.save_game_btn);
        promotionChoices = findViewById(R.id.pChoices);
        PlayActivity.promoteChoice = 'Q';
        String[] positions = {
                "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8", "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"
                , "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6", "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"
                , "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4", "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"
                , "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2", "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"};
        ImageView[] image_views = {
                findViewById(R.id.a8_piece), findViewById(R.id.b8_piece), findViewById(R.id.c8_piece), findViewById(R.id.d8_piece),
                findViewById(R.id.e8_piece), findViewById(R.id.f8_piece), findViewById(R.id.g8_piece), findViewById(R.id.h8_piece),
                findViewById(R.id.a7_piece), findViewById(R.id.b7_piece), findViewById(R.id.c7_piece), findViewById(R.id.d7_piece),
                findViewById(R.id.e7_piece), findViewById(R.id.f7_piece), findViewById(R.id.g7_piece), findViewById(R.id.h7_piece),
                findViewById(R.id.a6_piece), findViewById(R.id.b6_piece), findViewById(R.id.c6_piece), findViewById(R.id.d6_piece),
                findViewById(R.id.e6_piece), findViewById(R.id.f6_piece), findViewById(R.id.g6_piece), findViewById(R.id.h6_piece),
                findViewById(R.id.a5_piece), findViewById(R.id.b5_piece), findViewById(R.id.c5_piece), findViewById(R.id.d5_piece),
                findViewById(R.id.e5_piece), findViewById(R.id.f5_piece), findViewById(R.id.g5_piece), findViewById(R.id.h5_piece),
                findViewById(R.id.a4_piece), findViewById(R.id.b4_piece), findViewById(R.id.c4_piece), findViewById(R.id.d4_piece),
                findViewById(R.id.e4_piece), findViewById(R.id.f4_piece), findViewById(R.id.g4_piece), findViewById(R.id.h4_piece),
                findViewById(R.id.a3_piece), findViewById(R.id.b3_piece), findViewById(R.id.c3_piece), findViewById(R.id.d3_piece),
                findViewById(R.id.e3_piece), findViewById(R.id.f3_piece), findViewById(R.id.g3_piece), findViewById(R.id.h3_piece),
                findViewById(R.id.a2_piece), findViewById(R.id.b2_piece), findViewById(R.id.c2_piece), findViewById(R.id.d2_piece),
                findViewById(R.id.e2_piece), findViewById(R.id.f2_piece), findViewById(R.id.g2_piece), findViewById(R.id.h2_piece),
                findViewById(R.id.a1_piece), findViewById(R.id.b1_piece), findViewById(R.id.c1_piece), findViewById(R.id.d1_piece),
                findViewById(R.id.e1_piece), findViewById(R.id.f1_piece), findViewById(R.id.g1_piece), findViewById(R.id.h1_piece)};

        uiMap = new HashMap<String, ImageView>();
        for(int i = 0; i<positions.length;i++){
            image_views[i].setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {
                    int clicked_id =  v.getId();
                    for(String key : PlayActivity.uiMap.keySet()){
                        if(clicked_id == PlayActivity.uiMap.get(key).getId()){
                            //Log.d("Clicked", key);
                            if(PlayActivity.left_command == null){
                                //First click
                                //Log.d("Clicked", "First Click");
                                PlayActivity.left_command = key;
                                uiMap.get(key).setColorFilter(Color.rgb(0, 200, 0));
                            } else{
                                //Second click
                                uiMap.get(PlayActivity.left_command).clearColorFilter();
                                uiMap.get(key).clearColorFilter();
                                if(key.equalsIgnoreCase(PlayActivity.left_command)){
                                    //Clicked in place -> reset
                                    //Log.d("Clicked", "Second Click Reset");
                                    PlayActivity.left_command = null;
                                    PlayActivity.right_command = null;
                                } else{
                                    //Log.d("Clicked", "Second Click");
                                    PlayActivity.right_command = key;
                                    //Execute command
                                    if(PlayActivity.left_command != null && PlayActivity.right_command != null){
                                        try {
                                            executeCommand();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        } catch (ClassNotFoundException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            });
            uiMap.put(positions[i], image_views[i]);
        }
        disableSelection(true);

        promotionChoices.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton chosen = (RadioButton) group.findViewById(checkedId);
                String choice = chosen.getText().toString();
                //Log.d("Radio Change:", choice);
                switch (choice){
                    case "Queen":
                        PlayActivity.promoteChoice = 'Q';
                        break;
                    case "Rook":
                        PlayActivity.promoteChoice = 'R';
                        //Log.d("Choose:", PlayActivity.promoteChoice+"");
                        break;
                    case "Knight":
                        PlayActivity.promoteChoice = 'N';
                        break;
                    case "Bishop":
                        PlayActivity.promoteChoice = 'B';
                        break;
                    default:
                        PlayActivity.promoteChoice = 'Q';
                }
            }
        });

        start_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGame();
                disableSelection(false);
                hidePopup(true);
                hideSave(true);
                setEndGameType("");
                can_undo = true;
                try {
                    lastSavedBoard = b1.getCopy();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("Clicked", "Draw");
                PlayActivity.left_command = "draw";
                try {
                    executeCommand();
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                PlayActivity.left_command = null;
                PlayActivity.right_command = null;
            }
        });

        resign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("Clicked", "Resigned");
                PlayActivity.left_command = "resign";
                try {
                    executeCommand();
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                PlayActivity.left_command = null;
                PlayActivity.right_command = null;
            }
        });

        ai_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("Clicked", "button");
                PlayActivity.left_command = "ai";
                try {
                    executeCommand();
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                PlayActivity.left_command = null;
                PlayActivity.right_command = null;
            }
        });

        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PlayActivity.recorded_moves!= null){
                    if(PlayActivity.recorded_moves.size() > 0 && can_undo){
                        //Remove last move from recording
                        //Log.d("Undo", String.format("Removing from recording: %s", PlayActivity.recorded_moves.get(PlayActivity.recorded_moves.size()-1)));
                        PlayActivity.recorded_moves.remove(PlayActivity.recorded_moves.size()-1);
                        //Undo actual move logic
                        if(lastSavedBoard!= null){
                            try {
                                b1 = lastSavedBoard.getCopy();
                            } catch (IOException | ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                            b1.draw(uiMap);
                        }
                        can_undo = false;
                    }
                }
            }
        });

        yes_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidePopup(true);
                hideSave(false);
            }
        });

        no_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidePopup(true);
                //hideSave(false);
            }
        });

        save_game_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputLayout save_game_box = findViewById(R.id.save_title_box);
                String game_title = save_game_box.getEditText().getText().toString();
                //Log.d("Save Game:", String.format("Title: %s", game_title) );
                if(game_title.isEmpty()){
                        //Display error: empty title
                    Toast.makeText(PlayActivity.this, "Please enter a non-empty title", Toast.LENGTH_LONG).show();
                }else{
                    //Make recorded game
                    for(String move : PlayActivity.recorded_moves){
                        //Log.d("Print moves:", move);
                    }
                    RecordedGame new_game = new RecordedGame(game_title, LocalDateTime.now(), PlayActivity.recorded_moves);
                    MainActivity.getGamesList().add(new_game);
                    //Display game saved
                    hideSave(true);
                    Toast.makeText(PlayActivity.this, "Game Saved!", Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
    public void setEndGameType(String type){
        TextView end_type = findViewById(R.id.endgame_type);
        end_type.setText(type);
    }
    public void disableSelection(Boolean toDisable){
        for(String key : uiMap.keySet()){
            ImageView currpiece_ui = uiMap.get(key);
            currpiece_ui.setEnabled(!toDisable);
            currpiece_ui.clearColorFilter();
        }
        draw.setEnabled(!toDisable);
        resign.setEnabled(!toDisable);
        ai_button.setEnabled(!toDisable);
        undo.setEnabled(!toDisable);
    }
    public void hidePopup(Boolean toHide){
        ConstraintLayout popup = findViewById(R.id.popup_gameover);
        if(toHide){
            popup.setVisibility(View.INVISIBLE);
        } else{
            popup.setVisibility(View.VISIBLE);
        }
    }

    public void hideSave(Boolean toHide){
        ConstraintLayout popup = findViewById(R.id.save_game);
        if(toHide){
            popup.setVisibility(View.INVISIBLE);
        } else{
            popup.setVisibility(View.VISIBLE);
        }
    }


    public void executeCommand() throws IOException, ClassNotFoundException {

        if(b1.turn == 'w') {

               //Log.d("Game", "White's move: ");
        }else {
                //Log.d("Game", "Black's move: ");
        }
        String nextCommand;
        if(
                   !PlayActivity.left_command.equalsIgnoreCase("draw")
                && !PlayActivity.left_command.equalsIgnoreCase("resign")
                && !PlayActivity.left_command.equalsIgnoreCase("ai")) {
            nextCommand = PlayActivity.left_command + " " + PlayActivity.right_command;
        }else{
            nextCommand = PlayActivity.left_command;
        }

        //Log.d("Game", nextCommand);
        String[] commands = handleCommand(nextCommand, b1);
        if(commands == null) {
            b1.gameFinished = true;
            //Log.d("Game", "Game finished, terminating game ");
            disableSelection(true);
            hidePopup(false);
            return;
        }
        String src = commands[0];
        String dest = commands[1];
        lastSavedBoard = b1.getCopy();
        b1 = b1.getCopy();
        boolean commandExecuted = b1.executeCommand(src, dest);
        if(!commandExecuted) {
            //Log.d("Game", "Move NOT VALID ");
            Toast.makeText(this, "Invalid Move", Toast.LENGTH_SHORT).show();
            PlayActivity.left_command = null;
            PlayActivity.right_command = null;
        } else{
            can_undo = true;
            PlayActivity.left_command = null;
            PlayActivity.right_command = null;
            if(b1.checkmate) {
                b1.draw(uiMap);
                //Log.d("Game", "Checkmate");
                if (b1.wKcheck) {
                    //Log.d("Game", "Black Wins");
                    setEndGameType("Checkmate: Black Wins");
                }
                if (b1.bKcheck) {
                    //Log.d("Game", "White Wins");
                    setEndGameType("Checkmate: White Wins");
                }
                b1.gameFinished = true;
                disableSelection(true);
                hidePopup(false);
                return;
            }
            b1.draw(uiMap);
            if (b1.bKcheck || b1.wKcheck) {
                //Log.d("Game", "Check");
            }
            if(b1.turn == 'w') {
                b1.turn = 'b';
            }else {
                b1.turn = 'w';
            }
        }
    }

    public String[] handleCommand(String nextCommand, Board b1) {
        if(nextCommand.equalsIgnoreCase("resign")) {
            if(b1.turn == 'w') {
                setEndGameType("Black Wins");
                PlayActivity.recorded_moves.add(nextCommand);
                //System.out.println("Black wins");
            } else {
                setEndGameType("White Wins");
                PlayActivity.recorded_moves.add(nextCommand);
                //System.out.println("White wins");
            }
            return null;
        }
        if(nextCommand.equalsIgnoreCase("draw")) {
            //System.out.println("draw");
            setEndGameType("Draw");
            PlayActivity.recorded_moves.add(nextCommand);
            return null;
        }
        if(nextCommand.equalsIgnoreCase("ai")){
            return b1.aiMove().split(" ");
        }

        return nextCommand.split(" ");
    }
    /**
     * Checks that the requested promotion is a valid piece type
     * @param extraCommand input command
     * @return true if valid, false otherwise
     */
    public static boolean isValidPromoteCommand(String extraCommand) {
        if(extraCommand.equals("N")
                || extraCommand.equals("Q")
                || extraCommand.equals("B")
                || extraCommand.equals("R")
                || extraCommand.equals("p")) {
            return true;
        }
        return false;
    }
    /**
     * Main driver method, instantiates Board, handles turns, and receives/processes user input
     * Checks for various special events or commands, e.g promotes, draws, resigns, etc
     */

    //Scanner inputGetter;
    //inputGetter = new Scanner(System.in);
		/*if(debugScanner != null) {
			inputGetter = debugScanner;
		} else {
			inputGetter = new Scanner(System.in);
		}*/
        /*
        do {

        } while(gameFinished == false);
        inputGetter.close();*/

    public void startGame() {
        Board b1 = new Board();
        b1.init(0);
        b1.turn = 'w';
        b1.draw(uiMap);
        this.b1 = b1;
        PlayActivity.recorded_moves = new ArrayList<String>();
    }
}

