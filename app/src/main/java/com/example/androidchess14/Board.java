package com.example.androidchess14;

import android.util.Log;
import android.widget.ImageView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * Defines the chess board, including initialization, console output, and move processing
 * @author Calvin Li netID: cfl53
 * @author Andrew Cheng netID: ac1792
 * @author Section 1 Group 14
 */
public class Board implements Serializable {
    private static final long serialVersionUID = 1L;

    Boolean gameFinished;
    HashMap<String,Pos> currboard;
    /**
     * Array of all positions of the chess board
     */
    String[] posKeys =
            {"a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"
                    , "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"
                    , "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"
                    , "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"
                    , "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"
                    , "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"
                    , "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"
                    , "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"};
    /**
     * Initializes and saves the black king's position
     */
    String bKpos = "e8";
    /**
     * Initializes and saves the white king's position
     */
    String wKpos = "e1";
    /**
     * Checks if the black king is currently in check
     */
    boolean bKcheck = false;
    /**
     * Checks if the white king is currently in check
     */
    boolean wKcheck = false;
    /**
     * Checks if checkmate has occurred
     */
    boolean checkmate=false;
    /**
     * Holds the current player's turn
     */
    char turn = 'w';
    /**
     * Saves promotion target for white player promotion
     */
    String wPromote = null;
    /**
     * Saves promotion target for black player promotion
     */
    String bPromote = null;
    /**
     * Saves a pawn that can be taken via en passant
     */
    Piece passantPiece = null;

    Boolean didPromote = false;
    int activityType;
    /**
     * Draws the current board state and outputs it as ASCII onto console
     * This method iterates through posKeys, passing each key into the currboard hashMap and outputting each value
     * The output is an 8x8 chess board, displaying the current position of all pieces.
     *
     */
    List<String> moveList = new ArrayList<>();

    public Board getCopy() throws IOException, ClassNotFoundException {
        ByteArrayOutputStream out_stream = new ByteArrayOutputStream();
        ObjectOutputStream obj_out_stream = new ObjectOutputStream(out_stream);
        obj_out_stream.writeObject(this);
        byte[] byte_contents = out_stream.toByteArray();
        ByteArrayInputStream in_stream = new ByteArrayInputStream(byte_contents);
        ObjectInputStream obj_in_stream = new ObjectInputStream(in_stream);
        return (Board) obj_in_stream.readObject();
    }

    public void draw(HashMap<String, ImageView> uiMap) {
        HashMap<String, Integer> piece_map = new HashMap<String, Integer>();
        piece_map.put("  ",  android.R.color.transparent);
        piece_map.put("##",  android.R.color.transparent);
        piece_map.put("bp",  R.drawable.bp);
        piece_map.put("wp",  R.drawable.wp);
        piece_map.put("bB",  R.drawable.bb);
        piece_map.put("wB",  R.drawable.wb);
        piece_map.put("bN",  R.drawable.bn);
        piece_map.put("wN",  R.drawable.wn);
        piece_map.put("bR",  R.drawable.br);
        piece_map.put("wR",  R.drawable.wr);
        piece_map.put("bQ",  R.drawable.bq);
        piece_map.put("wQ",  R.drawable.wq);
        piece_map.put("bK",  R.drawable.bk);
        piece_map.put("wK",  R.drawable.wk);
        for(int i = 0; i<posKeys.length; i++){
            String currKey = posKeys[i];
            updateBoard(uiMap, piece_map,currKey);
        }
        /*System.out.println();
        for (int i = 1; i < posKeys.length+1; i++) {
            String currKey = posKeys[i-1];
            if( i % 8 == 0 ) {
                System.out.print(this.currboard.get(currKey) + " " + currKey.charAt(1) + "\n");
            }else {
                System.out.print(this.currboard.get(currKey) + " ");
            }
        }
        char[] toPrint = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
        for(int i = 0; i< 8; i++) {
            System.out.print(" " + toPrint[i] + " ");
        }
        System.out.println();
        System.out.println();*/


    }

    public void updateBoard(HashMap<String, ImageView> uiMap, HashMap<String, Integer> piece_map,  String currKey){
        Pos currPos = this.currboard.get(currKey);
        ImageView currImage = (ImageView) uiMap.get(currKey);
        currImage.setImageResource(piece_map.get(currPos.toString()));
    }
    public String aiMove(){
        ArrayList<String> allyPositions = new ArrayList<>();
        for (Map.Entry boardElement : currboard.entrySet()){
            String position = (String)boardElement.getKey();
            Pos allypos = (Pos)boardElement.getValue();
            if(allypos.currpiece!=null){
                if(allypos.currpiece.team == turn){
                    allyPositions.add(position);
                }
            }
        }
        Collections.shuffle(allyPositions);
        for(String randomPosition : allyPositions){
            Piece randomPiece = currboard.get(randomPosition).currpiece;
            ArrayList<String> randomMoves = new ArrayList<>();
            if (randomPiece instanceof Pawn){
                int currRank = Integer.parseInt(randomPosition.charAt(1) + "");
                if(turn == 'w') { // can move up 1 or 2
                    if(currRank == 2) {
                        if(currboard.get(getPos(randomPosition, "u", 1) ).currpiece == null
                                && currboard.get(getPos(randomPosition, "u", 2) ).currpiece == null) {
                            randomMoves.add(getPos(randomPosition, "u", 2));
                        }
                    }
                    if(currboard.get(getPos(randomPosition, "u", 1) ).currpiece == null) {
                        randomMoves.add(getPos(randomPosition, "u", 1));
                    }
                    Pos upperRightPos = currboard.get( getPos(randomPosition, "ur", 1));
                    if(upperRightPos != null) {
                        Piece upperRightPiece = upperRightPos.currpiece;
                        if(upperRightPiece != null && upperRightPiece.team == 'b') {
                            randomMoves.add(getPos(randomPosition, "ur", 1));
                        }
                    }
                    Pos upperLeftPos = currboard.get( getPos(randomPosition, "ul", 1));
                    if(upperLeftPos != null) {
                        Piece upperLeftPiece = upperLeftPos.currpiece;
                        if(upperLeftPiece != null && upperLeftPiece.team == 'b') {
                            randomMoves.add(getPos(randomPosition, "ul", 1));
                        }
                    }

                } else {
                    if(currRank == 7) {
                        if(currboard.get(getPos(randomPosition, "d", 1) ).currpiece == null
                                && currboard.get(getPos(randomPosition, "d", 2) ).currpiece == null) {
                            randomMoves.add(getPos(randomPosition, "d", 2));
                        }
                    }
                    if(currboard.get(getPos(randomPosition, "d", 1) ).currpiece == null) {
                        randomMoves.add(getPos(randomPosition, "d", 1));
                    }

                    Pos downRightPos = currboard.get(getPos(randomPosition, "dr", 1));
                    if(downRightPos != null) {
                        Piece downRightPiece = downRightPos.currpiece;
                        if(downRightPiece != null && downRightPiece.team == 'w') {
                            randomMoves.add(getPos(randomPosition, "dr", 1));
                        }
                    }

                    Pos downLeftPos = currboard.get(getPos(randomPosition, "dl", 1));
                    if(downLeftPos != null) {
                        Piece downLeftPiece = downLeftPos.currpiece;
                        if(downLeftPiece != null && downLeftPiece.team == 'w') {
                            randomMoves.add(getPos(randomPosition, "dl", 1));
                        }
                    }
                }
            }
            else if (randomPiece instanceof Knight){
                randomMoves.add(getPos(getPos(randomPosition, "u", 2), "r", 1));
                randomMoves.add(getPos(getPos(randomPosition, "r", 2), "u", 1));
                randomMoves.add(getPos(getPos(randomPosition, "d", 2), "r", 1));
                randomMoves.add(getPos(getPos(randomPosition, "r", 2), "d", 1));
                randomMoves.add(getPos(getPos(randomPosition, "u", 2), "l", 1));
                randomMoves.add(getPos(getPos(randomPosition, "l", 2), "u", 1));
                randomMoves.add(getPos(getPos(randomPosition, "d", 2), "l", 1));
                randomMoves.add(getPos(getPos(randomPosition, "l", 2), "d", 1));

            }
            else if (randomPiece instanceof Rook){
                for(int i = 1 ; i<8; i++) {
                    randomMoves.add(getPos(randomPosition, "u", i));
                    randomMoves.add(getPos(randomPosition, "r", i));
                    randomMoves.add(getPos(randomPosition, "d", i));
                    randomMoves.add(getPos(randomPosition, "l", i));
                }
            }
            else if (randomPiece instanceof Bishop){
                for(int i = 1 ; i<8; i++) {
                    randomMoves.add(getPos(randomPosition, "ur", i));
                    randomMoves.add(getPos(randomPosition, "ul", i));
                    randomMoves.add(getPos(randomPosition, "dr", i));
                    randomMoves.add(getPos(randomPosition, "dl", i));
                }
            }
            else if (randomPiece instanceof Queen){
                for(int i = 1 ; i<8; i++) {
                    randomMoves.add(getPos(randomPosition, "u", i));
                    randomMoves.add(getPos(randomPosition, "ur", i));
                    randomMoves.add(getPos(randomPosition, "r", i));
                    randomMoves.add(getPos(randomPosition, "dr", i));
                    randomMoves.add(getPos(randomPosition, "d", i));
                    randomMoves.add(getPos(randomPosition, "dl", i));
                    randomMoves.add(getPos(randomPosition, "l", i));
                    randomMoves.add(getPos(randomPosition, "ul", i));
                }

            }
            else if (randomPiece instanceof King){
                randomMoves.add(getPos(randomPosition, "u", 1));
                randomMoves.add(getPos(randomPosition, "ur", 1));
                randomMoves.add(getPos(randomPosition, "r", 1));
                randomMoves.add(getPos(randomPosition, "dr", 1));
                randomMoves.add(getPos(randomPosition, "d", 1));
                randomMoves.add(getPos(randomPosition, "dl", 1));
                randomMoves.add(getPos(randomPosition, "l", 1));
                randomMoves.add(getPos(randomPosition, "ul", 1));
            }
            Collections.shuffle(randomMoves);
            for(String dest : randomMoves){
                if(dest!=null){
                    //Log.d("Checking move",dest);
                    if(testMove(randomPosition, dest, currboard)){
                    //Log.d("Checking move","found");
                    return randomPosition +" "+dest;
                    }
                }

            }

        }
        return "Error NoMovesFound";
    }

    public void init(int activityType) {
        if(activityType==0){
            this.activityType=0;
        }
        else if(activityType==1){
            this.activityType=1;
        }
        HashMap<String,Pos> board = new HashMap<String,Pos>();
        String[] elements =
                {"bR", "bN", "bB", "bQ", "bK", "bB", "bN", "bR"
                        , "bp", "bp", "bp", "bp", "bp", "bp", "bp", "bp"
                        , "  ", "##", "  ", "##", "  ", "##", "  ", "##"
                        , "##", "  ", "##", "  ", "##", "  ", "##", "  "
                        , "  ", "##", "  ", "##", "  ", "##", "  ", "##"
                        , "##", "  ", "##", "  ", "##", "  ", "##", "  "
                        , "wp", "wp", "wp", "wp", "wp", "wp", "wp", "wp"
                        , "wR", "wN", "wB", "wQ", "wK", "wB", "wN", "wR"};
        boolean isWhite = true;
        for(int j = 0; j< 64; j++) {
            String currKey = posKeys[j];
            String currElement = elements[j];
            //System.out.println(currElement);
            if(board.containsKey(currKey)) {
                continue;
            }
            Pos currPos;
            if(j % 8 == 0 && j!= 0) {
                if( isWhite == true) {
                    isWhite = false;
                }else {
                    isWhite = true;
                }
            }
            if( isWhite == true && currElement.compareTo("  ") == 0) {
                currPos = new Pos("  ");
                isWhite = false;
            }
            else if(isWhite == false && currElement.compareTo("##") == 0) {
                currPos = new Pos("##");
                isWhite = true;
            }
            else {
                Piece newPiece = makePiece(currElement, currKey);
                if(isWhite == true) {
                    currPos = new Pos("  ", newPiece);
                    isWhite = false;
                }else {
                    currPos = new Pos("##", newPiece);
                    isWhite = true;
                }
            }
            board.put(currKey, currPos);
        }

        this.currboard = board;
        this.gameFinished = false;
    }

    public Piece makePiece(String name, String pos) {
        char team = name.charAt(0);
        char type = name.charAt(1);
        Piece newPiece;
        switch (type) {
            case 'K':
                newPiece = new King(name, team, pos);
                break;
            case 'Q':
                newPiece = new Queen(name, team, pos);
                break;
            case 'R':
                newPiece = new Rook(name, team, pos);
                break;
            case 'B':
                newPiece = new Bishop(name, team, pos);
                break;
            case 'N':
                newPiece = new Knight(name, team, pos);
                break;
            default:
                newPiece = new Pawn(name, team, pos);
                break;
        }
        return newPiece;
    }
    /**
     * Defines a Pos, which contains color and potentially holds a {iece
     * @see Piece
     */
    public class Pos implements Serializable{
        /**
         * The color of the Pos, black or white, saved as  "##" or "  "
         */
        String color;
        /**
         * The current Piece that the Pos holds. null if not holding a Piece
         */
        Piece currpiece;
        /**
         *  Constructor for a Pos that does not hold a Piece
         *  @param color the desired color of the Pos
         */
        public Pos(String color) {
            this.color = color;
            this.currpiece = null;
        }
        /**
         * Constructor for a Pos that holds a Piece
         * @param color the desired color of the Pos
         * @param currpiece the desired initial Piece
         */
        public Pos(String color, Piece currpiece) {
            this.color = color;
            this.currpiece = currpiece;
        }
        /**
         * toString method used for board output
         * @return name of Piece if one exists on this Pos, color otherwise
         */
        public String toString() {
            if(this.currpiece != null) {
                return currpiece.name;
            }
            else {
                return this.color;
            }
        }
    }
    /**
     * Declares common fields and methods for all chess piece types
     */
    public abstract class Piece implements Serializable {
        /**
         * name of Piece
         */
        String name; // Ex) bK -> black king
        /**
         * current position of Piece
         */
        String pos;
        /**
         * team of Piece
         */
        char team; // Ex) 'b' -> black
        /**
         * move processing, defined uniquely for each subclass
         * @param dest destination position
         * @param currboard hashMap of chessboard
         * @return true if move succeeds, false otherwise
         */
        public abstract boolean move(String dest, HashMap<String,Pos> currboard);
    }
    /**
     *  Returns true if any pieces exist between two positions of the board
     *  Applicable only if there is a space between the two positions, and if they are either
     *  diagonally, horizontally, or vertically oriented from each other
     *  Returns false if no pieces exist between two positions of the board
     *  @param currboard the hashMap storing all Pos objects
     *  @param src starting position
     *  @param dest destination position
     *  @return Whether or not the path is blocked
     */
    public boolean isBlocked(String src, String dest, HashMap<String,Pos> currboard) {
        char srcFile = src.charAt(0);
        int srcRank = Integer.parseInt(src.charAt(1) + "");
        char destFile = dest.charAt(0);
        int destRank = Integer.parseInt(dest.charAt(1) + "");
        boolean isBlocked = false;
        String direction;
        int dist;
        boolean canTake = false;
        Piece desPiece = currboard.get(dest).currpiece;
        Piece srcPiece = currboard.get(src).currpiece;
        if(desPiece != null &&  srcPiece != null) {
            if(desPiece.team != srcPiece.team) {
                //Can take
                canTake = true;
            }
        }

        if(srcFile == destFile) { // same x -> can move up or down
            dist = destRank - srcRank;
            if(Math.abs(dist) == 1 && currboard.get(src).currpiece != null) {
                if(currboard.get(src).currpiece.name.charAt(1) != 'p') {
                    return false;
                }
            }
            if(dist > 0) {
                direction = "u";
            } else {
                direction = "d";
            }
            for (int i = 1; i < Math.abs(dist) + 1; i++) {
                Piece currPiece = currboard.get(getPos(src, direction, i)).currpiece;
                if(currPiece != null) {
                    //Have a piece in path
                    if(desPiece != null) {
                        if(currPiece.name == desPiece.name && canTake == true) {
                            break;
                        }
                    }
                    isBlocked = true;
                    break;
                }
            }
        } else if(srcRank == destRank) { // same y -> can move left or right
            dist = (int) destFile - (int) srcFile;
            if(Math.abs(dist) == 1) {
                return false;
            }
            else if(dist > 0) {
                direction = "r";
            } else {
                direction = "l";
            }
            for (int i = 1; i < Math.abs(dist) + 1; i++) {
                Piece currPiece = currboard.get(getPos(src, direction, i)).currpiece;
                if(currPiece != null) {
                    //Have a piece in path
                    if(desPiece != null) {
                        if(currPiece.name == desPiece.name && canTake == true) {
                            break;
                        }
                    }
                    isBlocked = true;
                    break;
                }
            }
        } else{ // Diagonal
            dist = (int) destFile - (int) srcFile;
            int rankdist = destRank - srcRank;
            if(Math.abs(dist) == 1) {
                return false;
            }
            else if(dist > 0 && rankdist > 0) { // upper right
                direction = "ur";
            } else if(dist > 0 && rankdist < 0) { // lower right
                direction = "dr";
            } else if(dist < 0 && rankdist > 0) { // upper left
                direction = "ul";
            } else { // lower left
                direction = "dl";
            }
            for (int i = 1; i < Math.abs(dist) + 1; i++) {
                Piece currPiece = currboard.get(getPos(src, direction, i)).currpiece;
                if(currPiece != null) {
                    //Have a piece in path
                    if(desPiece != null) {
                        if(currPiece.name == desPiece.name && canTake == true) {
                            break;
                        }
                    }
                    isBlocked = true;
                    break;
                }
            }
        }
        return isBlocked;
    }
    /**
     * Processes input move, and returns a boolean indicating its success or failure
     * Uses the first coordinate to find the appropriate piece, returning false if
     * no piece exists on that position
     * Attempts to move the appropriate piece to target destination, returning false if move is found to be illegal
     * @param src source position
     * @param dest destination position
     * @return true if command succeeds, false otherwise
     */
    public boolean executeCommand(String src, String dest) {
        Piece currPiece = this.currboard.get(src).currpiece;
        if(currPiece == null) {
            System.out.println("Illegal move, try again");
            return false;
        } else {
            boolean moveExecuted = currPiece.move(dest, this.currboard);
            if(activityType==0){
                if(moveExecuted == true && !didPromote){
                PlayActivity.recorded_moves.add(String.format("%s %s", src, dest));
            } else if(moveExecuted == true && didPromote){
                PlayActivity.recorded_moves.add(String.format("%s %s %s", src, dest, PlayActivity.promoteChoice));
                didPromote = false;
            }
            }
            return moveExecuted;
        }
    }


    public String getPos(String currentPos, String direction, int numSpaces) {
        if(currentPos == null) {
            return null;
        }
        char file = currentPos.charAt(0);
        int rank = Integer.parseInt(currentPos.charAt(1) + "") ;
        String newPos;
        int newFileInt;
        char newFileChar;
        int newRank;
        switch (direction) {
            case "u":
                newRank = rank + numSpaces;
                if(newRank > 8) {
                    newPos = null;
                }else {
                    newPos = file + "" + (int) (newRank) + "";
                }
                break;
            case "ur":
                newRank = rank  + numSpaces;
                newFileInt = (int) file + numSpaces;
                if(newRank > 8 || newFileInt > 104) {
                    newPos = null;
                } else {
                    newFileChar = (char) newFileInt;
                    newPos =  newFileChar + "" +  (int) (newRank) + "";
                }
                break;
            case "r":
                newFileInt = (int) file + numSpaces; //Max 104 -> h
                if(newFileInt > 104) {
                    newPos = null;
                }else {
                    newFileChar = (char) newFileInt;
                    newPos = newFileChar + "" + rank + "";
                }
                break;
            case "dr":
                newRank = rank  - numSpaces;
                newFileInt = (int) file + numSpaces;
                if(newRank < 1 || newFileInt > 104) {
                    newPos = null;
                } else {
                    newFileChar = (char) newFileInt;
                    newPos =  newFileChar + "" +  (int) (newRank) + "";
                }
                break;
            case "d":
                newRank = rank - numSpaces;
                if(newRank < 1) {
                    newPos = null;
                } else {
                    newPos = file + "" + (int) (newRank) + "";
                }
                break;
            case "dl":
                newRank = rank  - numSpaces;
                newFileInt = (int) file - numSpaces;
                if(newRank < 1 || newFileInt < 97) {
                    newPos = null;
                } else {
                    newFileChar = (char) newFileInt;
                    newPos =  newFileChar + "" +  (int) (newRank) + "";
                }
                break;
            case "l":
                newFileInt = (int) file - numSpaces; // min 97 -> a
                if(newFileInt < 97) {
                    newPos = null;
                } else {
                    newFileChar = (char) newFileInt;
                    newPos = newFileChar + "" + rank + "";
                }
                break;
            case "ul":
                newRank = rank  + numSpaces;
                newFileInt = (int) file - numSpaces;
                if(newRank > 8 || newFileInt < 97) {
                    newPos = null;
                } else {
                    newFileChar = (char) newFileInt;
                    newPos =  newFileChar + "" +  (int) (newRank) + "";
                }
                break;
            default:
                newPos = null;
                break;
        }
        return newPos;
    }


    public boolean isInBounds(String dest, HashMap<String,Pos> currboard) {
        if(currboard.containsKey(dest)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Executes the move directed by the user, provided it is legal
     * Removes the Piece from the Pos at source position,
     * and adds it to the Pos at destination position
     * Updates various flags depending on current board state
     * e.g Promotions, checks, checkmate
     * @param src source position
     * @param dest destination position
     * @param currboard hashMap of chess board
     */
    public void executeMove( String src, String dest, HashMap<String,Pos> currboard) {
        moveList.add(src + "," + dest);
        passantPiece = null;
        Pos srcPos = currboard.get(src);
        Piece srcPiece = srcPos.currpiece;
        srcPiece.pos = dest;
        Pos destPos = currboard.get(dest);
        if(srcPiece!= null) {
            if(srcPiece.name.equals("wp") || srcPiece.name.equals("bp")) {
                Pawn currPawn = (Pawn) srcPiece;
                if(currPawn.wasPromoted == true) {
                    srcPiece = currPawn.promotedPiece;
                }
            }
        }
        destPos.currpiece = srcPiece;
        srcPos.currpiece = null;
        if(srcPiece instanceof King) {
            if(srcPiece.team == 'w') {
                wKpos = srcPiece.pos;
            }  else {
                bKpos = srcPiece.pos;
            }
        }

        if(inDanger(wKpos,'w', 0).pos.equalsIgnoreCase("safe") == true && wKcheck == true) {
            wKcheck = false;
        }
        else if(inDanger(bKpos,'b', 0).pos.equalsIgnoreCase("safe") == true && bKcheck == true) {
            bKcheck = false;
        }
        //System.out.println("wKpos: " + wKpos);
        //System.out.println("wKcheck: " + wKcheck);
        //System.out.println("bKpos: " + bKpos);
        //System.out.println("bKcheck: " + bKcheck);

        if(wKcheck== true || bKcheck==true) {
            inCheckmate();
        }

    }

    public void inCheckmate() {
        String kingpos=""; String kingdest =""; char attackerTeam = 'a';
        if (wKcheck) {kingpos = wKpos; turn = 'w'; attackerTeam = 'b';};
        if (bKcheck) {kingpos=bKpos; turn = 'b'; attackerTeam = 'w';};
        String[] kingDir = new String[] {"u","ur","r","dr","d","dl","l","ul"};
        for (String dir : kingDir) {
            kingdest = getPos(kingpos,dir ,1);
            if(kingdest!=null) {
                //System.out.println("Kingpos: " + kingpos );
                //System.out.println("Kingdest: " + kingdest );
                if (testMove(kingpos,kingdest, currboard)){
                    if (wKcheck) {turn = 'b';};
                    if (bKcheck) {turn = 'w';};
                    return;
                }
            }
        }
        DangerInfo attackerInfo = inDanger(kingpos,turn,0);
        int i = 0;
        String defendingPos="";
        do{
            defendingPos = inDanger(attackerInfo.pos,attackerTeam,i).pos;
            if (defendingPos.equals("safe")){break;}
            if (testMove(defendingPos,attackerInfo.pos, currboard)){
                //System.out.println("Not in checkmate");
                if (wKcheck) {turn = 'b';};
                if (bKcheck) {turn = 'w';};
                return;
            }
            i++;
        }while (!defendingPos.equals("safe"));
        if (attackerInfo.blockable) {
            String blockingPos="";
            char kingFile = kingpos.charAt(0);
            int kingRank = Integer.parseInt(kingpos.charAt(1) + "");
            char attackerFile = attackerInfo.pos.charAt(0);
            int attackerRank = Integer.parseInt(attackerInfo.pos.charAt(1) + "");
            ArrayList<String> blockablePos = new ArrayList<String>();
            String dir = "";
            int xDist = attackerFile-kingFile;
            int yDist = attackerRank-kingRank;
            int spacesBetween = Math.max(Math.abs(xDist), Math.abs(yDist))-1;
            if (xDist == 0) {
                if (yDist>0) dir = "u";
                else dir = "d";
            }
            else if (yDist ==0) {
                if (xDist>0) dir = "r";
                else dir = "l";
            }
            else if (xDist>0 && yDist>0)
                dir = "ur";
            else if (xDist<0 && yDist>0)
                dir = "ul";
            else if (xDist>0 && yDist<0)
                dir = "dr";
            else if (xDist<0 && yDist<0)
                dir = "dl";
            for(int j = 1; j<=spacesBetween;j++) {
                blockablePos.add(getPos(kingpos,dir,j));
            }
            int k;
            String possiblePawn = "";
            for (String space : blockablePos) {
                k=0;
                do{
                    blockingPos = inDanger(space,attackerTeam,k).pos;
                    if (!blockingPos.equals("safe")) {
                        while (currboard.get(blockingPos).currpiece instanceof Pawn) {
                            k++;
                            blockingPos=inDanger(space,attackerTeam,k).pos;
                            if (blockingPos.equals("safe")) break;
                        }
                    }
                    if (blockingPos.equals("safe")){break;}
                    if (testMove(blockingPos,space, currboard)){
                        if (wKcheck) {turn = 'b';};
                        if (bKcheck) {turn = 'w';};
                        return;
                    }
                    k++;
                }while(!blockingPos.equals("safe"));

                if(blockingPos.equals("safe")) {
                    //System.out.println("space: " + space);
                    if(attackerTeam == 'w') {
                        possiblePawn=getPos(space, "u",1);
                        if (possiblePawn!=null) {
                            if (currboard.get(possiblePawn).currpiece!=null) {
                                if(currboard.get(possiblePawn).currpiece instanceof Pawn) {
                                    blockingPos = possiblePawn;
                                }
                            }
                            else if(Integer.parseInt(space.charAt(1)+"")==5 ) {
                                possiblePawn=getPos(space, "u",2);
                                if(currboard.get(possiblePawn).currpiece instanceof Pawn) {
                                    blockingPos = possiblePawn;
                                }
                            }
                        }

                    }
                    if(attackerTeam == 'b') {
                        possiblePawn=getPos(space, "d",1);
                        if (possiblePawn!=null) {
                            if (currboard.get(possiblePawn).currpiece!=null) {
                                if(currboard.get(possiblePawn).currpiece instanceof Pawn) {
                                    blockingPos = possiblePawn;
                                }
                            }
                            else if(Integer.parseInt(space.charAt(1)+"") == 4 ) {
                                possiblePawn=getPos(space, "d",2);
                                if(currboard.get(possiblePawn).currpiece instanceof Pawn) {
                                    blockingPos = possiblePawn;
                                }
                            }
                        }

                    }
                    //System.out.println("blockingPos: " + blockingPos);
                    //System.out.println("space:" + space);
                    if(blockingPos.equalsIgnoreCase("safe") == false) {
                        if (testMove(blockingPos,space, currboard)){
                            if (wKcheck) {turn = 'b';};
                            if (bKcheck) {turn = 'w';};
                            return;
                        }
                    }
                }
            }
        }
        checkmate=true;
        return;
    }
    /**
     * Simulates a move to find if it is valid or not. Validity depends on the following factors:
     * Whether the destination is occupied by a piece that cannot be taken, and if it puts the allied king in danger
     * Furthermore, it has additional flags for special moves, e.g moves that place the opposing king in danger
     * @param src source position of Piece to move
     * @param dest destination position of Piece to move
     * @param currboard hashMap representing chessboard
     * @return true if move passes all checks, false otherwise
     */
    public boolean testMove( String src, String dest, HashMap<String,Pos> currboard) {
        String allyKing="";
        String oppoKing="";
        char oppoturn = 'a';
        if (turn =='w') {
            allyKing= wKpos;
            oppoKing= bKpos;
            oppoturn = 'b';
        }
        if (turn =='b') {
            allyKing = bKpos;
            oppoKing = wKpos;
            oppoturn = 'w';
        }
        boolean validMove=true;
        Pos srcPos = currboard.get(src);
        Piece currPiece = srcPos.currpiece;
        Piece temp = currboard.get(dest).currpiece;
        if (!(currPiece.team==turn)||!isInBounds(dest,currboard)){
            return validMove=false;
        }
        if(temp!=null) {
            if(temp.team==turn && currPiece != temp) {
                //System.out.println("allied piece in destination");
                return validMove=false;
            }
        }
        if (!(currPiece instanceof Knight)) {
            if(isBlocked(src, dest, currboard)) {
                return validMove=false;
            }
        }

        srcPos.currpiece = null;
        currPiece.pos = dest;
        Pos destPos = currboard.get(dest);
        destPos.currpiece = currPiece;
        char movedKing = 'n';
        if(currPiece.name.equalsIgnoreCase("bK")) {
            bKpos = dest;
            movedKing = 'b';
            allyKing = bKpos;
            oppoKing = wKpos;
        } else if(currPiece.name.equalsIgnoreCase("wK")) {
            wKpos = dest;
            movedKing = 'w';
            allyKing = wKpos;
            oppoKing = bKpos;
        }
        if (inDanger(allyKing,turn,0).pos.equalsIgnoreCase("safe") == false){
            //System.out.println(inDanger(allyKing,turn,0).pos);
            validMove=false;
            currPiece.pos = src;
            srcPos.currpiece = currPiece;
            destPos.currpiece=temp;
            if(movedKing == 'b') {
                bKpos = src;
            } else if (movedKing == 'w') {
                wKpos = src;
            }
            return validMove;
        }
        //String test = inDanger(oppoKing,oppoKing.charAt(0),0).pos;
        //	System.out.println(test);
        //System.out.println(test.equals("safe"));
        if(!(inDanger(oppoKing,oppoturn,0).pos.equals("safe"))) {
            if (oppoturn=='w')
                wKcheck=true;
            else
                bKcheck=true;
        }
        currPiece.pos = src;
        srcPos.currpiece = currPiece;
        destPos.currpiece=temp;
        if(movedKing == 'b') {
            bKpos = src;
        } else if (movedKing == 'w') {
            wKpos = src;
        }
        return validMove;
    }
    /**
     * Subclass of Piece that represents a king chess piece
     * @see Piece
     */
    public class King extends Piece{
        /**
         * Whether this king has been moved
         */
        boolean moved;
        /**
         * The Rook that the king can castle to
         */
        Piece castleRook;
        /**
         * The position the Rook will be after castling completes
         */
        String castleRookEndPos;
        /**
         * Constructor for a King object
         * @param name name of King piece
         * @param team team of King piece
         * @param pos current position of King piece
         */
        public King(String name, char team, String pos) {
            super();
            this.name = team + "K";
            this.pos = pos;
            this.team = team;
            this.moved = false;
        }
        /**
         * Adds castling as a potentially valid move for the player to make, provided conditions are met
         * Checks if either the king or rook has previously moved, if the path is bocked, and if the castling takes the king
         * out of, through, or into check
         * @param dest destination position of king
         * @param possibleMoves list of possible moves the king can execute
         * @param args the positions the king moves through that could possibly cause it to go into check
         * @return true if addition was successful, false otherwise
         */
        public boolean addCastleMove(String dest, ArrayList<String> possibleMoves , String[] args) {
            Piece potentialRook;
            boolean isCheck;
            if(this.team == 'w') {
                isCheck = wKcheck;
            } else {
                isCheck = bKcheck;
            }
            if(dest.equalsIgnoreCase(args[0]) && this.moved == false) {
                potentialRook = currboard.get(getPos(dest, args[1], Integer.parseInt(args[2]))).currpiece;
                if(potentialRook != null) {
                    if(potentialRook.name.equalsIgnoreCase(args[3])){
                        Rook castleRook = (Rook) potentialRook;
                        if(castleRook.moved == false
                                && isBlocked(this.pos, args[4], currboard) == false
                                && inDanger(args[5],team, 0).pos.equalsIgnoreCase("safe")
                                && inDanger(args[6],team, 0).pos.equalsIgnoreCase("safe")
                                && isCheck == false) {
                            possibleMoves.add(dest);
                            this.castleRook = potentialRook;
                            this.castleRookEndPos = args[5];
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        @Override
        /**
         * Algorithm to implement king movement
         * Adds all possible moves to a list, and attempts to match it with user input
         * If match succeeds, simulates the move to see if it is valid
         * If move is valid, executes the move and adjusts relevant flags
         * @return true if move succeeds, false otherwise
         * @see addCastleMove
         * @see testMove
         */
        public boolean move(String dest, HashMap<String,Pos> currboard) {
            //If in check -> Need to move king
            ArrayList<String> possibleMoves = new ArrayList<String>();
            possibleMoves.add(getPos(this.pos, "u", 1));
            possibleMoves.add(getPos(this.pos, "ur", 1));
            possibleMoves.add(getPos(this.pos, "r", 1));
            possibleMoves.add(getPos(this.pos, "dr", 1));
            possibleMoves.add(getPos(this.pos, "d", 1));
            possibleMoves.add(getPos(this.pos, "dl", 1));
            possibleMoves.add(getPos(this.pos, "l", 1));
            possibleMoves.add(getPos(this.pos, "ul", 1));
            boolean castle = false;
            if(this.team == 'w' && turn == 'w') {
                //Check right or left castle positions
                String[] argsR = {"g1", "r", "1", "wR", dest, "f1", "g1"};
                String[] argsL = {"c1", "l", "2", "wR", getPos(this.pos, "l", 3), "d1", "c1"};
                if(addCastleMove(dest, possibleMoves, argsR) == true || addCastleMove(dest, possibleMoves, argsL) == true) {
                    castle = true;
                }

            } else if(this.team == 'b' && turn == 'b') {
                String[] argsR = {"g8", "r", "1", "bR", dest, "f8", "g8"};
                String[] argsL = {"c8", "l", "2", "bR", getPos(this.pos, "l", 3), "d8", "c8"};
                if(addCastleMove(dest, possibleMoves, argsR) == true || addCastleMove(dest, possibleMoves, argsL) == true) {
                    castle = true;
                }
            }
            //System.out.println(castle);
            if(possibleMoves.contains(dest) && castle == true) {
                if(this.castleRook != null && this.castleRookEndPos != null) {
                    if(testMove(this.pos, dest, currboard) == true && testMove(this.castleRook.pos, this.castleRookEndPos, currboard) == true) {
                        this.moved = true;
                        Rook currRook = (Rook) this.castleRook;
                        currRook.moved = true;
                        executeMove(this.pos, dest, currboard);
                        executeMove(this.castleRook.pos, this.castleRookEndPos, currboard);
                        String oppoKing ="";
                        char oppoturn = '|';
                        if (this.team =='w') {
                            oppoKing= bKpos;
                            oppoturn = 'b';
                        }
                        if (this.team =='b') {
                            oppoKing = wKpos;
                            oppoturn = 'w';
                        }
                        if(!(inDanger(oppoKing,oppoturn,0).pos.equals("safe"))) {
                            if (oppoturn=='w')
                                wKcheck=true;
                            else
                                bKcheck=true;
                        }
                        if(wKcheck== true || bKcheck==true) {
                            inCheckmate();
                        }
                        return true;
                    } else {
                        System.out.println("Illegal move, try again");
                        return false;
                    }
                } else {
                    System.out.println("Illegal move, try again");
                    return false;
                }
            }
            else if(possibleMoves.contains(dest) && castle == false) {
                if(testMove(this.pos, dest, currboard)) {
                    this.moved = true;
                    executeMove(this.pos, dest, currboard);
                    return true;
                } else {
                    System.out.println("Illegal move, try again");
                    return false;
                }

            }
            else {
                System.out.println("Illegal move, try again");
                return false;
            }

        }
    }
    /**
     * Subclass of Piece that represents a queen chess piece
     * @see Piece
     *
     */
    public class Queen extends Piece{
        /**
         * Constructor for a Queen object
         * @param name name of Queen
         * @param team team of Queen
         * @param pos current position of Queen
         */
        public Queen(String name, char team, String pos) {
            super();
            this.name = team + "Q";
            this.pos = pos;
            this.team = team;
        }

        @Override
        /**
         * Algorithm to implement queen movement
         * Adds all possible moves the queen can make into a list, and attempts to
         * match the user's input with it.
         * If a match is found, simulates the move to see if it is valid.
         * If so, executes the move and updates relevant flags
         * @return true if move succeeds, false otherwise
         * @see testMove
         */
        public boolean move(String dest, HashMap<String,Pos> currboard) {
            ArrayList<String> possibleMoves = new ArrayList<String>();
            for(int i = 1 ; i<8; i++) {
                possibleMoves.add(getPos(this.pos, "u", i));
                possibleMoves.add(getPos(this.pos, "ur", i));
                possibleMoves.add(getPos(this.pos, "r", i));
                possibleMoves.add(getPos(this.pos, "dr", i));
                possibleMoves.add(getPos(this.pos, "d", i));
                possibleMoves.add(getPos(this.pos, "dl", i));
                possibleMoves.add(getPos(this.pos, "l", i));
                possibleMoves.add(getPos(this.pos, "ul", i));
            }

            if(possibleMoves.contains(dest)) {
                if(testMove(this.pos, dest, currboard)) {
                    executeMove(this.pos, dest, currboard);
                    return true;
                } else {
                    System.out.println("Illegal move, try again");
                    return false;
                }

            } else {
                System.out.println("Illegal move, try again");
                return false;
            }
        }
    }
    /**
     * Subclass of Piece that simulates a rook chess piece
     * @see Piece
     */
    public class Rook extends Piece{
        /**
         * Whether the rook has moved
         */
        boolean moved;
        /**
         * Constructor for a Rook object
         * @param name name of Rook
         * @param team team of Rook
         * @param pos current position of Rook
         */
        public Rook(String name, char team, String pos) {
            super();
            this.name = team + "R";
            this.pos = pos;
            this.team = team;
            this.moved = false;
        }

        @Override
        /**
         * Algorithm to implement rook movement
         * Adds all possible moves the rook can make into a list, and attempts to
         * match the user's input with it.
         * If a match is found, simulates the move to see if it is valid.
         * If so, executes the move and updates relevant flags
         * @return true if move succeeds, false otherwise
         * @see testMove
         * @see King
         */
        public boolean move(String dest, HashMap<String,Pos> currboard) {
            ArrayList<String> possibleMoves = new ArrayList<String>();
            for(int i = 1 ; i<8; i++) {
                possibleMoves.add(getPos(this.pos, "u", i));
                possibleMoves.add(getPos(this.pos, "r", i));
                possibleMoves.add(getPos(this.pos, "d", i));
                possibleMoves.add(getPos(this.pos, "l", i));
            }
            if(possibleMoves.contains(dest)) {
                if(testMove(this.pos, dest, currboard)) {
                    executeMove(this.pos, dest, currboard);
                    this.moved = true;
                    return true;
                } else {
                    System.out.println("Illegal move, try again");
                    return false;
                }

            } else {
                System.out.println("Illegal move, try again");
                return false;
            }
        }
    }
    /**
     * Subclass of Piece that simulates a bishop chess piece
     * @see Piece
     */
    public class Bishop extends Piece{
        /**
         * Constructor for a Bishop object
         * @param name name of Bishop
         * @param team team of Bishop
         * @param pos current position of Bishop
         */
        public Bishop(String name, char team, String pos) {
            super();
            this.name = team + "B";
            this.pos = pos;
            this.team = team;
        }

        @Override
        /**
         * Algorithm to implement bishop movement
         * Adds all possible moves the bishop can make into a list, and attempts to
         * match the user's input with it.
         * If a match is found, simulates the move to see if it is valid.
         * If so, executes the move and updates relevant flags
         * @return true if move succeeds, false otherwise
         * @see testMove
         */
        public boolean move(String dest, HashMap<String,Pos> currboard) {
            ArrayList<String> possibleMoves = new ArrayList<String>();
            for(int i = 1 ; i<8; i++) {
                possibleMoves.add(getPos(this.pos, "ur", i));
                possibleMoves.add(getPos(this.pos, "ul", i));
                possibleMoves.add(getPos(this.pos, "dr", i));
                possibleMoves.add(getPos(this.pos, "dl", i));
            }
            if(possibleMoves.contains(dest)) {
                if(testMove(this.pos, dest, currboard)) {
                    executeMove(this.pos, dest, currboard);
                    return true;
                } else {
                    System.out.println("Illegal move, try again");
                    return false;
                }

            } else {
                System.out.println("Illegal move, try again");
                return false;
            }
        }
    }
    /**
     * Subclass of Piece that simulates a knight chess piece
     */
    public class Knight extends Piece{
        /**
         * Constructor for a Knight object
         * @param name name of Knight
         * @param team team of Knight
         * @param pos current position of Knight
         */
        public Knight(String name, char team, String pos) {
            super();
            this.name = team + "N";
            this.pos = pos;
            this.team = team;
        }

        @Override
        /**
         * Algorithm to implement knight movement
         * Adds all possible moves the knight can make into a list, and attempts to
         * match the user's input with it.
         * If a match is found, simulates the move to see if it is valid.
         * If so, executes the move and updates relevant flags
         * @return true if move succeeds, false otherwise
         * @see testMove
         */
        public boolean move(String dest, HashMap<String,Pos> currboard) {
            ArrayList<String> possibleMoves = new ArrayList<String>();
            possibleMoves.add(getPos(getPos(this.pos, "u", 2), "r", 1));
            possibleMoves.add(getPos(getPos(this.pos, "r", 2), "u", 1));
            possibleMoves.add(getPos(getPos(this.pos, "d", 2), "r", 1));
            possibleMoves.add(getPos(getPos(this.pos, "r", 2), "d", 1));
            possibleMoves.add(getPos(getPos(this.pos, "u", 2), "l", 1));
            possibleMoves.add(getPos(getPos(this.pos, "l", 2), "u", 1));
            possibleMoves.add(getPos(getPos(this.pos, "d", 2), "l", 1));
            possibleMoves.add(getPos(getPos(this.pos, "l", 2), "d", 1));
            if(possibleMoves.contains(dest)) {
                if(testMove(this.pos, dest, currboard)) {
                    executeMove(this.pos, dest, currboard);
                    return true;
                } else {
                    System.out.println("Illegal move, try again");
                    return false;
                }

            } else {
                System.out.println("Illegal move, try again");
                return false;
            }
        }
    }
    /**
     * Subclass of Piece that simulates a pawn chess piece
     */
    public class Pawn extends Piece{
        /**
         * Checks if pawn is promoted
         */
        boolean wasPromoted;
        /**
         * Stores what the pawn attempts to promote into
         */
        Piece promotedPiece;
        /**
         * Constructor for a Knight object
         * @param name name of Knight
         * @param team team of Knight
         * @param pos current position of Knight
         */
        public Pawn(String name, char team, String pos) {
            super();
            this.name = team + "p";
            this.pos = pos;
            this.team = team;
            this.wasPromoted = false;
            this.promotedPiece = null;
        }

        @Override
        public boolean move(String dest, HashMap<String,Pos> currboard) {
            ArrayList<String> possibleMoves = new ArrayList<String>();
            int currRank = Integer.parseInt(this.pos.charAt(1) + "");
            int destRank = Integer.parseInt(dest.charAt(1) + "");
            boolean addedDoubleMove = false;
            boolean addedPassant = false;
            boolean isRightPassant = false;
            boolean isLeftPassant = false;
            if(this.team == 'w') { // can move up 1 or 2
                if(currRank == 2) {
                    if(currboard.get(getPos(this.pos, "u", 1) ).currpiece == null
                            && currboard.get(getPos(this.pos, "u", 2) ).currpiece == null) {
                        possibleMoves.add(getPos(this.pos, "u", 2));
                        addedDoubleMove = true;
                    }
                }
                if(currboard.get(getPos(this.pos, "u", 1) ).currpiece == null) {
                    possibleMoves.add(getPos(this.pos, "u", 1));
                }
                Pos upperRightPos = currboard.get( getPos(this.pos, "ur", 1));
                if(upperRightPos != null) {
                    Piece upperRightPiece = upperRightPos.currpiece;
                    if(upperRightPiece != null && upperRightPiece.team == 'b') {
                        possibleMoves.add(getPos(this.pos, "ur", 1));
                    }
                }
                Pos upperLeftPos = currboard.get( getPos(this.pos, "ul", 1));
                if(upperLeftPos != null) {
                    Piece upperLeftPiece = upperLeftPos.currpiece;
                    if(upperLeftPiece != null && upperLeftPiece.team == 'b') {
                        possibleMoves.add(getPos(this.pos, "ul", 1));
                    }
                }
                if(destRank == 6 && currRank == 5 && passantPiece != null) {
                    isRightPassant = passantPiece.pos.equalsIgnoreCase(getPos(this.pos, "r", 1));
                    isLeftPassant = passantPiece.pos.equalsIgnoreCase(getPos(this.pos, "l", 1));
                    if(passantPiece.name.equalsIgnoreCase("bp")) {
                        if(isRightPassant) {
                            possibleMoves.add(getPos(this.pos, "ur", 1));
                            addedPassant = true;
                        } else if (isLeftPassant) {
                            possibleMoves.add(getPos(this.pos, "ul", 1));
                            addedPassant = true;
                        }
                    }
                }

            } else {
                if(currRank == 7) {
                    if(currboard.get(getPos(this.pos, "d", 1) ).currpiece == null
                            && currboard.get(getPos(this.pos, "d", 2) ).currpiece == null) {
                        possibleMoves.add(getPos(this.pos, "d", 2));
                        addedDoubleMove = true;
                    }
                }
                if(currboard.get(getPos(this.pos, "d", 1) ).currpiece == null) {
                    possibleMoves.add(getPos(this.pos, "d", 1));
                }

                Pos downRightPos = currboard.get(getPos(this.pos, "dr", 1));
                if(downRightPos != null) {
                    Piece downRightPiece = downRightPos.currpiece;
                    if(downRightPiece != null && downRightPiece.team == 'w') {
                        possibleMoves.add(getPos(this.pos, "dr", 1));
                    }
                }

                Pos downLeftPos = currboard.get(getPos(this.pos, "dl", 1));
                if(downLeftPos != null) {
                    Piece downLeftPiece = downLeftPos.currpiece;
                    if(downLeftPiece != null && downLeftPiece.team == 'w') {
                        possibleMoves.add(getPos(this.pos, "dl", 1));
                    }
                }
                if(destRank == 3 && currRank == 4 && passantPiece != null) {
                    isRightPassant = passantPiece.pos.equalsIgnoreCase(getPos(this.pos, "r", 1));
                    isLeftPassant = passantPiece.pos.equalsIgnoreCase(getPos(this.pos, "l", 1));
                    if(passantPiece.name.equalsIgnoreCase("wp")) {
                        if(isRightPassant) {
                            possibleMoves.add(getPos(this.pos, "dr", 1));
                            addedPassant = true;
                        } else if (isLeftPassant) {
                            possibleMoves.add(getPos(this.pos, "dl", 1));
                            addedPassant = true;
                        }
                    }
                }
            }

            if(currRank == 7 && destRank > currRank) {
                String newPieceName;
                if(wPromote !=null) {
                    newPieceName = 'w'+ wPromote;
                    wPromote = null;
                } else {
                    if(!PlayActivity.left_command.equalsIgnoreCase("ai")){
                        newPieceName = 'w'+ Character.toString(PlayActivity.promoteChoice);
                        //Log.d("Promote", newPieceName);
                        wPromote = null;
                    } else{
                        newPieceName = "wQ";
                    }
                }
                //Make new piece at destination pos
                this.promotedPiece = makePiece(newPieceName, dest);
                this.wasPromoted = true;
            } else if( currRank == 2 && destRank < currRank) {
                String newPieceName;
                if(bPromote !=null) {
                    newPieceName = 'b' + bPromote;
                    bPromote = null;
                } else {
                    if(PlayActivity.left_command.equalsIgnoreCase("ai") == false){
                        newPieceName = 'b'+ Character.toString(PlayActivity.promoteChoice);
                        //Log.d("Promote", newPieceName);
                        bPromote = null;
                    } else{
                        newPieceName = "bQ";
                    }
                }
                this.promotedPiece = makePiece(newPieceName, dest);
                this.wasPromoted = true;
            } else {
                if(wPromote != null) {
                    wPromote = null;
                }
                if(bPromote != null) {
                    bPromote = null;
                }
            }
            if(possibleMoves.contains(dest)
                    && addedPassant	 == true
                    && testMove(this.pos, dest, currboard) == true
                    && isBlocked(this.pos, dest, currboard) == false){
                Piece tempPiece = null;
                if(isRightPassant == true) {
                    tempPiece = currboard.get(getPos(this.pos, "r", 1)).currpiece;
                } else if(isLeftPassant == true) {
                    tempPiece = currboard.get(getPos(this.pos, "l", 1)).currpiece;
                }

                if(tempPiece !=null) {
                    String tempPos = tempPiece.pos;
                    currboard.get(tempPos).currpiece = null;
                    if(testMove(this.pos, dest, currboard) == true) {
                        executeMove(this.pos, dest, currboard);
                        return true;
                    }else {
                        currboard.get(tempPos).currpiece = tempPiece;
                    }
                }

            }
            if(possibleMoves.contains(dest)
                    && this.wasPromoted == true
                    && testMove(this.pos, dest, currboard) == true
                    && isBlocked(this.pos, dest, currboard) == false) {
                if(this.promotedPiece!=null) {
                    //Test move with temp piece:
                    Piece tempPiece = currboard.get(dest).currpiece;
                    currboard.get(dest).currpiece = promotedPiece;
                    if(testMove(dest, dest, currboard) == true) {
                        executeMove(this.pos, dest, currboard);
                        didPromote = true;
                        return true;
                    } else {
                        currboard.get(dest).currpiece = tempPiece;
                        System.out.println("[1] Illegal move, try again");
                        return false;
                    }
                }
                else {
                    System.out.println("Illegal move, try again");
                    return false;
                }
            }
            else if(possibleMoves.contains(dest) && wasPromoted == false && addedPassant == false) {
                if(testMove(this.pos, dest, currboard) && isBlocked(this.pos, dest, currboard) == false) {
                    executeMove(this.pos, dest, currboard);
                    if(addedDoubleMove == true) {
                        passantPiece= currboard.get(this.pos).currpiece;
                    }
                    return true;
                } else {
                    System.out.println("Illegal move, try again");
                    return false;
                }

            } else {
                System.out.println("Illegal move, try again");
                return false;
            }
        }
    }

  
    public class DangerInfo {
        /**
         * Position of attacking piece
         */
        String pos;
        /**
         * Whether the attacking piece can be blocked
         */
        boolean blockable;
        public DangerInfo(String pos, boolean blockable){
            this.pos=pos;
            this.blockable=blockable;
        }
    }

    public String knightChecker (char team, String pos) {
        String knightPos=pos;
        if (pos == null)
            return knightPos;
        if (team == 'w') {
            if(currboard.get(pos).currpiece!=null) {
                if(currboard.get(pos).currpiece.name.equals("bN")) {
                    return knightPos;
                }

            }
        }
        else if (team == 'b') {
            if(currboard.get(pos).currpiece!=null) {
                if(currboard.get(pos).currpiece.name.equals("wN")) {
                    return knightPos;
                }

            }
        }
        return null;
    }

    public DangerInfo inDanger (String pos,char team, int skip) {
        ArrayList<String> cycle = new ArrayList<String>(Arrays.asList("u","ur","r","dr","d","dl","l","ul"));
        String dir = "";
        ListIterator<String> cycleItr = cycle.listIterator();
        DangerInfo danger = new DangerInfo("safe",true);
        if (currboard.get(pos).currpiece!=null) {
            if (currboard.get(pos).currpiece instanceof Pawn) {
                Piece checkPassant = currboard.get(pos).currpiece;
                if(passantPiece!= null) {
                    if (passantPiece.name.equals(checkPassant.name)) {
                        String[] dangerPassant = {getPos(pos, "r",1),getPos(pos,"l",1)};
                        for (int p = 0; p<2;p++) {
                            if (currboard.get(dangerPassant[p])!=null) {
                                if(currboard.get(dangerPassant[p]).currpiece instanceof Pawn) {
                                    if (currboard.get(dangerPassant[p]).currpiece.team != team) {
                                        if (skip>0) {
                                            skip--;
                                        }
                                        else {
                                            danger.pos=dangerPassant[p];
                                            danger.blockable=false;
                                            return danger;
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        String currSearch = "";
        int i =1;
        String knightInfo = "";
        ArrayList<String> knightPositions =new ArrayList<String>(Arrays.asList(
                getPos(getPos(pos, "u", 2), "r", 1),
                getPos(getPos(pos, "r", 2), "u", 1),
                getPos(getPos(pos, "d", 2), "r", 1),
                getPos(getPos(pos, "r", 2), "d", 1),
                getPos(getPos(pos, "u", 2), "l", 1),
                getPos(getPos(pos, "l", 2), "u", 1),
                getPos(getPos(pos, "d", 2), "l", 1),
                getPos(getPos(pos, "l", 2), "d", 1)));
        for (String knight : knightPositions) {
            Piece currKnightPiece = currboard.get(pos).currpiece;
            if(currKnightPiece == null) {
                continue;
            }
            knightInfo = knightChecker(currKnightPiece.team,knight);
            if (knightInfo!=null) {
                if (skip>0) {
                    skip--;
                }
                else {
                    danger.pos=knight;
                    danger.blockable=false;
                    return danger;
                }
            }
        }
        //Piece currPosPiece = currboard.get(pos).currpiece;
        //if(currPosPiece != null) {
        if (team=='w') {
            while (i<8 && cycle.size()>0) {
                //System.out.println(i);
                while(cycleItr.hasNext()) {
                    dir = cycleItr.next();
                    //System.out.println(dir);
                    currSearch = getPos(pos, dir, i);
                    if (isInBounds(currSearch, currboard)) {
                        if(currboard.get(currSearch).currpiece!=null) {
                            if(i==1) {
                                if(dir.equals("u")  || dir.equals("r")  || dir.equals("d")  || dir.equals("l") ) {
                                    if(currboard.get(currSearch).currpiece.team =='w'||currboard.get(currSearch).currpiece.name.equals("bB")||currboard.get(currSearch).currpiece.name.equals("bp")||currboard.get(currSearch).currpiece.name.equals("bN")) {
                                        cycleItr.remove();
                                    }
                                    else if(currboard.get(currSearch).currpiece.name.equals("bK")||currboard.get(currSearch).currpiece.name.equals("bR")||currboard.get(currSearch).currpiece.name.equals("bQ")) {
                                        if(skip>0) {
                                            skip--;
                                            cycleItr.remove();
                                        }
                                        else {
                                            danger.pos=currSearch;
                                            danger.blockable=false;
                                            return danger;
                                        }
                                    }
                                }
                                else if(dir.equals("ul") || dir.equals("ur")) {
                                    if(currboard.get(currSearch).currpiece.team =='w'||currboard.get(currSearch).currpiece.name.equals("bN")||currboard.get(currSearch).currpiece.name.equals("bR")) {
                                        cycleItr.remove();
                                    }
                                    if(currboard.get(currSearch).currpiece.name.equals("bB")  || currboard.get(currSearch).currpiece.name.equals("bQ") ||currboard.get(currSearch).currpiece.name.equals("bp") ||currboard.get(currSearch).currpiece.name.equals("bK") ) {
                                        if(skip>0) {
                                            skip--;
                                            cycleItr.remove();
                                        }
                                        else {
                                            danger.pos=currSearch;
                                            danger.blockable=false;
                                            return danger;
                                        }
                                    }
                                }
                                else if(dir.equals("dl") || dir.equals("dr")) {
                                    if(currboard.get(currSearch).currpiece.team =='w'||currboard.get(currSearch).currpiece.name.equals("bp")||currboard.get(currSearch).currpiece.name.equals("bN")||currboard.get(currSearch).currpiece.name.equals("bR")) {
                                        cycleItr.remove();
                                    }
                                    if(currboard.get(currSearch).currpiece.name.equals("bB")  || currboard.get(currSearch).currpiece.name.equals("bQ") ||currboard.get(currSearch).currpiece.name.equals("bK") ) {
                                        if(skip>0) {
                                            skip--;
                                            cycleItr.remove();
                                        }
                                        else {
                                            danger.pos=currSearch;
                                            danger.blockable=false;
                                            return danger;
                                        }
                                    }
                                }
                            }
                            else {
                                if(dir.equals("u")  || dir.equals("r")  || dir.equals("d")  || dir.equals("l") ) {
                                    if(currboard.get(currSearch).currpiece.team=='w'||currboard.get(currSearch).currpiece.name.equals("bK")||currboard.get(currSearch).currpiece.name.equals("bp")||currboard.get(currSearch).currpiece.name.equals("bN")||currboard.get(currSearch).currpiece.name.equals("bB")) {
                                        cycleItr.remove();
                                    }
                                    else if(currboard.get(currSearch).currpiece.name.equals("bR") || currboard.get(currSearch).currpiece.name.equals("bQ") ) {
                                        if(skip>0) {
                                            skip--;
                                            cycleItr.remove();
                                        }
                                        else {
                                            danger.pos=currSearch;
                                            return danger;
                                        }
                                    }
                                }
                                if (dir.equals("ur")  || dir.equals("ul") ||dir.equals("dr") || dir.equals("dl") ) {
                                    if(currboard.get(currSearch).currpiece.team == 'w'||currboard.get(currSearch).currpiece.name.equals("bK")||currboard.get(currSearch).currpiece.name.equals("bN")||currboard.get(currSearch).currpiece.name.equals("bp")||currboard.get(currSearch).currpiece.name.equals("bR")) {
                                        cycleItr.remove();
                                    }
                                    else if(currboard.get(currSearch).currpiece.name.equals("bB")  || currboard.get(currSearch).currpiece.name.equals("bQ") ) {

                                        if(skip>0) {
                                            skip--;
                                            cycleItr.remove();
                                        }
                                        else {
                                            danger.pos=currSearch;
                                            return danger;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        cycleItr.remove();
                    }
                }
                cycleItr=cycle.listIterator();
                i++;
            }

        }

        if (team =='b') {
            while (i<8 && cycle.size()>0) {
                while(cycleItr.hasNext()) {
                    dir = cycleItr.next();
                    currSearch = getPos(pos, dir, i);
                    if (isInBounds(currSearch, currboard)) {
                        if(currboard.get(currSearch).currpiece!=null) {
                            if(i==1) {
                                if(dir.equals("u") || dir.equals("r")|| dir.equals("d") || dir.equals("l")) {
                                    if(currboard.get(currSearch).currpiece.team == 'b' ||currboard.get(currSearch).currpiece.name.equals("wB")||currboard.get(currSearch).currpiece.name.equals("wp")||currboard.get(currSearch).currpiece.name.equals("wN")) {
                                        cycleItr.remove();
                                    }
                                    else if(currboard.get(currSearch).currpiece.name.equals("wK")||currboard.get(currSearch).currpiece.name.equals("wR")||currboard.get(currSearch).currpiece.name.equals("wQ")) {
                                        if(skip>0) {
                                            skip--;
                                            cycleItr.remove();
                                        }
                                        else {
                                            danger.pos=currSearch;
                                            danger.blockable=false;
                                            return danger;
                                        }
                                    }
                                }
                                else if(dir.equals("ul") || dir.equals("ur") ) {
                                    if(currboard.get(currSearch).currpiece.team == 'b' || currboard.get(currSearch).currpiece.name.equals("wN")||currboard.get(currSearch).currpiece.name.equals("wR") || currboard.get(currSearch).currpiece.name.equals("wp")) {
                                        cycleItr.remove();
                                    }
                                    if(currboard.get(currSearch).currpiece.name.equals("wB") || currboard.get(currSearch).currpiece.name.equals("wQ")||currboard.get(currSearch).currpiece.name.equals("wK")) {
                                        if(skip>0) {
                                            skip--;
                                            cycleItr.remove();
                                        }
                                        else {
                                            danger.pos=currSearch;
                                            danger.blockable=false;
                                            return danger;
                                        }
                                    }
                                }
                                else if(dir.equals("dl")  || dir.equals("dr")) {
                                    if(currboard.get(currSearch).currpiece.team == 'b' ||currboard.get(currSearch).currpiece.name.equals("wN")||currboard.get(currSearch).currpiece.name.equals("wR")) {
                                        cycleItr.remove();
                                    }
                                    if(currboard.get(currSearch).currpiece.name.equals("wB")  || currboard.get(currSearch).currpiece.name.equals("wQ") ||currboard.get(currSearch).currpiece.name.equals("wK") ||currboard.get(currSearch).currpiece.name.equals("wp") ) {
                                        if(skip>0) {
                                            skip--;
                                            cycleItr.remove();
                                        }
                                        else {
                                            danger.pos=currSearch;
                                            danger.blockable=false;
                                            return danger;
                                        }
                                    }
                                }
                            }
                            else {
                                if(dir.equals("u") || dir.equals("r") || dir.equals("d")|| dir.equals("l")) {
                                    if(currboard.get(currSearch).currpiece.team == 'b' ||currboard.get(currSearch).currpiece.name.equals("wK")||currboard.get(currSearch).currpiece.name.equals("wp")||currboard.get(currSearch).currpiece.name.equals("wN")||currboard.get(currSearch).currpiece.name.equals("wB")) {
                                        cycleItr.remove();
                                    }
                                    else if(currboard.get(currSearch).currpiece.name.equals("wR")  || currboard.get(currSearch).currpiece.name.equals("wQ") ) {
                                        if(skip>0) {
                                            skip--;
                                            cycleItr.remove();
                                        }
                                        else {
                                            danger.pos=currSearch;
                                            return danger;
                                        }
                                    }
                                }
                                else if (dir.equals("ur")  || dir.equals("ul") ||dir.equals("dr")  || dir.equals("dl") ) {
                                    if(currboard.get(currSearch).currpiece.team == 'b' ||currboard.get(currSearch).currpiece.name.equals("wK")||currboard.get(currSearch).currpiece.name.equals("wN")||currboard.get(currSearch).currpiece.name.equals("wp")||currboard.get(currSearch).currpiece.name.equals("wR")) {
                                        cycleItr.remove();
                                    }

                                    else if(currboard.get(currSearch).currpiece.name.equals("wB") || currboard.get(currSearch).currpiece.name.equals("wQ")) {
                                        if(skip>0) {
                                            skip--;
                                            cycleItr.remove();
                                        }
                                        else {
                                            danger.pos=currSearch;
                                            danger.blockable=true;
                                            return danger;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        cycleItr.remove();
                    }
                }
                cycleItr=cycle.listIterator();
                i++;
            }

        }
        //}
        return danger;
    }


}
