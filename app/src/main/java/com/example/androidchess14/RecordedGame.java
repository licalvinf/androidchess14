package com.example.androidchess14;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class RecordedGame implements Serializable {
    private static final long serialVersionUID = 1L;
    String title;
    LocalDateTime date;
    List<String> moveList;
    RecordedGame(String title, LocalDateTime date){
        this.title=title;
        this.date=date;
        moveList = new ArrayList<>();
    }
    RecordedGame(String title, LocalDateTime date, List<String> moveList){
        this(title, date);
        this.moveList=moveList;
    }
    public String toString(){
        return title + "\nPlayed: " + date.toLocalDate().toString();
    }
}
