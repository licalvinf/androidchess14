package com.example.androidchess14;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Button buttonPlay; private Button buttonWatch;
    private static List<RecordedGame> gamesList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setGamesList();
        try {
            gamesList = (List<RecordedGame>) InternalStorage.readObject(this,"data");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonPlay = (Button) findViewById(R.id.buttonPlay);
        buttonWatch = (Button) findViewById(R.id.buttonWatch);
        buttonPlay.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(),PlayActivity.class);
            startActivity(i);
        });
        buttonWatch.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(),WatchActivity.class);
            startActivity(i);
        });
    }
    public void setGamesList() {
        gamesList = new ArrayList<>();
    }
    public static List<RecordedGame> getGamesList(){
        return gamesList;
    }
    @Override
    protected void onPause() {
        super.onPause();
        try {
            InternalStorage.writeObject(this,"data",gamesList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}