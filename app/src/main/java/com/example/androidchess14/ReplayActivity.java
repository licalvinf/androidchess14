package com.example.androidchess14;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ReplayActivity extends AppCompatActivity {
    Button next;
    protected Board b1;
    protected static HashMap<String, ImageView> uiMap;
    private List<String> moveList;
    private int turnCount;
    private List<RecordedGame> gameList = MainActivity.getGamesList();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("Start of replay", gameList.get(0).toString());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replay);
        next = findViewById(R.id.next_button);
        moveList = WatchActivity.getMoveList();
        turnCount = 0;
        String[] positions = {
                "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8", "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"
                , "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6", "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"
                , "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4", "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"
                , "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2", "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"};
        ImageView[] image_views = {
                findViewById(R.id.a8_piece), findViewById(R.id.b8_piece), findViewById(R.id.c8_piece), findViewById(R.id.d8_piece),
                findViewById(R.id.e8_piece), findViewById(R.id.f8_piece), findViewById(R.id.g8_piece), findViewById(R.id.h8_piece),
                findViewById(R.id.a7_piece), findViewById(R.id.b7_piece), findViewById(R.id.c7_piece), findViewById(R.id.d7_piece),
                findViewById(R.id.e7_piece), findViewById(R.id.f7_piece), findViewById(R.id.g7_piece), findViewById(R.id.h7_piece),
                findViewById(R.id.a6_piece), findViewById(R.id.b6_piece), findViewById(R.id.c6_piece), findViewById(R.id.d6_piece),
                findViewById(R.id.e6_piece), findViewById(R.id.f6_piece), findViewById(R.id.g6_piece), findViewById(R.id.h6_piece),
                findViewById(R.id.a5_piece), findViewById(R.id.b5_piece), findViewById(R.id.c5_piece), findViewById(R.id.d5_piece),
                findViewById(R.id.e5_piece), findViewById(R.id.f5_piece), findViewById(R.id.g5_piece), findViewById(R.id.h5_piece),
                findViewById(R.id.a4_piece), findViewById(R.id.b4_piece), findViewById(R.id.c4_piece), findViewById(R.id.d4_piece),
                findViewById(R.id.e4_piece), findViewById(R.id.f4_piece), findViewById(R.id.g4_piece), findViewById(R.id.h4_piece),
                findViewById(R.id.a3_piece), findViewById(R.id.b3_piece), findViewById(R.id.c3_piece), findViewById(R.id.d3_piece),
                findViewById(R.id.e3_piece), findViewById(R.id.f3_piece), findViewById(R.id.g3_piece), findViewById(R.id.h3_piece),
                findViewById(R.id.a2_piece), findViewById(R.id.b2_piece), findViewById(R.id.c2_piece), findViewById(R.id.d2_piece),
                findViewById(R.id.e2_piece), findViewById(R.id.f2_piece), findViewById(R.id.g2_piece), findViewById(R.id.h2_piece),
                findViewById(R.id.a1_piece), findViewById(R.id.b1_piece), findViewById(R.id.c1_piece), findViewById(R.id.d1_piece),
                findViewById(R.id.e1_piece), findViewById(R.id.f1_piece), findViewById(R.id.g1_piece), findViewById(R.id.h1_piece)};
        uiMap = new HashMap<String, ImageView>();
        for(int i = 0; i<positions.length;i++) {
            uiMap.put(positions[i], image_views[i]);
        }
        startGame();
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("next", "clicked");
                MoveHandler(turnCount);
                turnCount++;
            }

        });
    }
    public void disableSelection(Boolean toDisable){
        next.setEnabled(!toDisable);
    }
    public void setEndGameType(String type){
        TextView end_type = findViewById(R.id.endgame_type);
        end_type.setText(type);
    }
    public void hidePopup(Boolean toHide){
        ConstraintLayout popup = findViewById(R.id.popup_gameover);
        if(toHide){
            popup.setVisibility(View.INVISIBLE);
        } else{
            popup.setVisibility(View.VISIBLE);
        }
    }

    private void MoveHandler(int turnCount) {
        Log.d("MoveHandler", "active");

        if( moveList == null || moveList.get(turnCount)==null){
            setEndGameType("Game ended");
            b1.gameFinished=true;
            disableSelection(true);
            hidePopup(false);
            return;
        }

        if (moveList.get(turnCount).equalsIgnoreCase("resign")){
            if(b1.turn == 'w') {
                setEndGameType("Black Wins");
                //System.out.println("Black wins");
            } else {
                setEndGameType("White Wins");
                //System.out.println("White wins");
            }
            b1.gameFinished = true;
            disableSelection(true);
            hidePopup(false);
            return;
        }
        else if(moveList.get(turnCount).equalsIgnoreCase("draw")){
            setEndGameType("Draw");
            b1.gameFinished = true;
            disableSelection(true);
            hidePopup(false);
            return;
        }
        String[] moves = moveList.get(turnCount).split(" ");
        String src = moves[0];
        String dest = moves[1];
        //Log.d("Turncount", String.valueOf(turnCount));
        //Log.d("Move1", src);
        //Log.d("Move2",dest);
        String promote;
        //Log.d("moves line:", moves.length+"");
        if(moves.length>2){
            promote = moves[2];
            //Log.d("Watch promote:", promote);
            if(b1.turn == 'w'){
                b1.wPromote = promote;
            } else if(b1.turn == 'b'){
                b1.bPromote = promote;
            }
        }
        b1.executeCommand(src, dest);
        if(b1.checkmate) {
            b1.draw(uiMap);
            //Log.d("Game", "Checkmate");
            if (b1.wKcheck) {
                //Log.d("Game", "Black Wins");
                setEndGameType("Checkmate: Black Wins");
            }
            if (b1.bKcheck) {
                //Log.d("Game", "White Wins");
                setEndGameType("Checkmate: White Wins");
            }
            b1.gameFinished = true;
            disableSelection(true);
            hidePopup(false);
            return;
        }
        b1.draw(uiMap);
        if (b1.bKcheck || b1.wKcheck) {
            //Log.d("Game", "Check");
        }
        if(b1.turn == 'w') {
            b1.turn = 'b';
        }else {
            b1.turn = 'w';
        }
        if( moveList.get(turnCount+1)==null){
            setEndGameType("Game ended");
            b1.gameFinished=true;
            disableSelection(true);
            hidePopup(false);
            return;
        }
    }

    public void startGame() {
        Board b1 = new Board();
        b1.init(1);
        b1.turn = 'w';
        b1.draw(uiMap);
        this.b1 = b1;
    }
}